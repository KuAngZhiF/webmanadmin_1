<?php
/**
 * FunAdmin
 * ============================================================================
 * 版权所有 2017-2028 FunAdmin，并保留所有权利。
 * 网站地址: https://www.FunAdmin.com
 * ----------------------------------------------------------------------------
 * 采用最新Thinkphp6实现
 * ============================================================================
 * Author: yuege
 * Date: 2019/10/3
 */
declare(strict_types=1);

use fun\addons\middleware\Addons;
use fun\addons\Service;
use fun\helper\FileHelper;
use think\Exception;
use think\facade\Db;
use think\facade\App;
use think\facade\Config;
use think\facade\Event;
use think\facade\Route;
use think\facade\Cache;
use think\helper\{
    Str, Arr
};

define('DS', DIRECTORY_SEPARATOR);


if (!function_exists('parse_name')) {
    /**
     * 字符串命名风格转换
     * type 0 将Java风格转换为C的风格 1 将C风格转换为Java的风格
     * @param string $name    字符串
     * @param int    $type    转换类型
     * @param bool   $ucfirst 首字母是否大写（驼峰规则）
     * @return string
     */
    function parse_name(string $name, int $type = 0, bool $ucfirst = true): string
    {
        if ($type) {
            $name = preg_replace_callback('/_([a-zA-Z])/', function ($match) {
                return strtoupper($match[1]);
            }, $name);

            return $ucfirst ? ucfirst($name) : lcfirst($name);
        }

        return strtolower(trim(preg_replace('/[A-Z]/', '_\\0', $name), '_'));
    }
}
/**
 * 判断文件或目录是否有写的权限
 */
function is_really_writable($file)
{
    if (DIRECTORY_SEPARATOR == '/' && @ ini_get("safe_mode") == false) {
        return is_writable($file);
    }
    if (!is_file($file) || ($fp = @fopen($file, "r+")) === false) {
        return false;
    }
    fclose($fp);
    return true;
}




// Form别名
if (!class_exists('Form')) {
    class_alias('fun\\Form', 'Form');
}

if (!function_exists('form_config')) {
    /**
     * @param string $name
     * @param string $type
     * @param array $options
     * @param '' $value
     * @return string
     */
    function form_config($name='',$options=[],$value='')
    {
        return Form::config($name, $options,$value);
    }
}

if (!function_exists('form_token')) {
    /**
     * @param string $name
     * @param string $type
     * @param array $options
     * @param '' $value
     * @return string
     */
    function form_token($name = '__token__', $type = 'md5')
    {
        return Form::token($name , $type);
    }
}

if (!function_exists('form_input')) {
    /**
     * @param string $name
     * @param string $type
     * @param array $options
     * @param '' $value
     * @return string
     */
    function form_input($name = '', $type = 'text', $options = [], $value = '')
    {
        return Form::input($name, $type, $options, $value);
    }
}

if (!function_exists('form_text')) {
    /**
     * @param $name
     * @param $options
     * @param $value
     * @return string
     */
    function form_text($name = '', $options = [], $value = '')
    {
        return Form::text($name,$options, $value);
    }
}
if (!function_exists('form_password')) {
    /**
     * @param $name
     * @param $options
     * @param $value
     * @return string
     */
    function form_password($name = '', $options = [], $value = '')
    {
        return Form::password($name,$options, $value);
    }
}
if (!function_exists('form_hidden')) {
    /**
     * @param $name
     * @param $options
     * @param $value
     * @return string
     */
    function form_hidden($name = '', $options = [], $value = '')
    {
        return Form::hidden($name,$options, $value);
    }
}
if (!function_exists('form_number')) {
    /**
     * @param $name
     * @param $options
     * @param $value
     * @return string
     */
    function form_number($name = '', $options = [], $value = '')
    {
        return Form::number($name,$options, $value);
    }
}
if (!function_exists('form_range')) {
    /**
     * @param $name
     * @param $options
     * @param $value
     * @return string
     */
    function form_range($name = '', $options = [], $value = '')
    {
        return Form::range($name,$options, $value);
    }
}
if (!function_exists('form_url')) {
    /**
     * @param $name
     * @param $options
     * @param $value
     * @return string
     */
    function form_url($name = '', $options = [], $value = '')
    {
        return Form::url($name,$options, $value);
    }
}
if (!function_exists('form_tel')) {
    /**
     * @param $name
     * @param $options
     * @param $value
     * @return string
     */
    function form_tel($name = '', $options = [], $value = '')
    {
        return Form::tel($name,$options, $value);
    }
}


if (!function_exists('form_email')) {
    /**
     * @param $name
     * @param $options
     * @param $value
     * @return string
     */
    function form_email($name = '', $options = [], $value = '')
    {
        return Form::email($name,$options, $value);
    }
}
if (!function_exists('form_rate')) {
    /**
     * 评分
     * @param string $name
     * @param array $options
     * @param '' $value
     * @return string
     */
    function form_rate($name = '', $options = [], $value = '')
    {
        return Form::rate($name, $options, $value);
    }
}

if (!function_exists('form_slider')) {
    /**
     * 滑块
     * @param string $name
     * @param array $options
     * @param '' $value
     * @return string
     */
    function form_slider($name = '', $options = [], $value = '')
    {
        return Form::slider($name, $options, $value);
    }
}
if (!function_exists('form_radio')) {
    /**
     * @param '' $name
     * @param '' $radiolist
     * @param array $options
     * @param string $value
     * @return string
     */
    function form_radio($name = '', $radiolist = '', $options = [], $value = '')
    {
        return Form::radio($name, $radiolist, $options, $value);
    }
}
if (!function_exists('form_switchs')) {
    /**
     * @param $name
     * @param $switch
     * @param $option
     * @param $value
     * @return string
     */
    function form_switchs($name='', $switch = [], $option = [], $value = '')
    {
        return Form::switchs($name, $switch, $option, $value);
    }
}
if (!function_exists('form_switch')) {
    /**
     * @param $name
     * @param $switch
     * @param $option
     * @param $value
     * @return string
     */
    function form_switch($name='', $switch = [], $option = [], $value = '')
    {
        return Form::switchs($name, $switch, $option, $value);
    }
}
if (!function_exists('form_checkbox')) {
    /**
     * @param $name
     * @return string
     */
    function form_checkbox($name ='', $list = [], $option = [], $value = '')
    {
        return Form::checkbox($name, $list, $option, $value);
    }
}

if (!function_exists('form_arrays')) {
    /**
     * @param $name
     * @return string
     */
    function form_arrays($name='', $list = [], $option = [])
    {
        return Form::arrays($name, $list, $option);
    }
}


if (!function_exists('form_textarea')) {
    /**
     * @param $name
     * @return string
     */
    function form_textarea($name = '', $option = [], $value = '')
    {
        return Form::textarea($name, $option, $value);
    }
}
if (!function_exists('form_select')) {
    /**
     * @param '' $name
     * @param array $options
     * @return string
     */
    function form_select($name = '', $select = [], $options = [], $attr = '', $value = '')
    {
        if (!empty($attr) and !is_array($attr)) $attr = explode(',', $attr);
        if (!empty($value) and !is_array($value)) $value = explode(',', (string)$value);
        return Form::multiselect($name, $select, $options, $attr, $value);
    }
}
if (!function_exists('form_multiselect')) {
    /**
     * @param $name
     * @param $select
     * @param $options
     * @param $attr
     * @param $value
     * @return string
     */
    function form_multiselect($name = '', $select = [], $options = [], $attr = '', $value = '')
    {
        if (!empty($attr) and !is_array($attr)) $attr = explode(',', $attr);
        return Form::multiselect($name, $select, $options, $attr, $value);
    }
}
if (!function_exists('form_selectplus')) {
    /**
     * @param $name
     * @param $select
     * @param $options
     * @param $attr
     * @param $value
     * @return string
     */
    function form_selectplus($name = '', $select = [], $options = [], $attr = '', $value = '')
    {
        if (!empty($attr) and !is_array($attr)) $attr = explode(',', $attr);
        return Form::selectplus($name, $select, $options, $attr, $value);
    }
}
if (!function_exists('form_selectn')) {
    /**
     * @param $name
     * @param $select
     * @param $options
     * @param $attr
     * @param $value
     * @return string
     */
    function form_selectn($name = '', $select = [], $options = [], $attr = '', $value = '')
    {
        if (!empty($attr) and !is_array($attr)) $attr = explode(',', $attr);
        return Form::selectn($name, $select, $options, $attr, $value);
    }
}
if (!function_exists('form_xmselect')) {
    /**
     * @param '' $name
     * @param array $options
     * @return string
     */
    function form_xmselect($name = '', $select = [], $options = [], $attr = '', $value = '')
    {
        if (!empty($attr) and is_array($attr)) $attr = implode(',', $attr);
        return Form::xmselect($name, $select, $options, $attr, $value);
    }
}
if (!function_exists('form_icon')) {
    /**
     * @param array $options
     * @return string
     */

    function form_icon($name = '', $options = [], $value = '')
    {
        return Form::icon($name, $options, $value);
    }
}

if (!function_exists('form_date')) {
    /**
     * @param array $options
     * @return string
     */

    function form_date($name = '', $options = [], $value = '')
    {
        return Form::date($name, $options, $value);
    }
}

if (!function_exists('form_city')) {
    /**
     * @param array $options
     * @return string
     */

    function form_city($name = 'cityPicker', $options = [])
    {
        return Form::city($name, $options);
    }
}
if (!function_exists('form_region')) {
    /**
     * @param array $options
     * @return string
     */

    function form_region($name = 'regionCheck', $options = [])
    {
        return Form::region($name, $options);
    }
}
if (!function_exists('form_tags')) {
    /**
     * @param array $options
     * @return string
     */

    function form_tags($name = '', $options = [], $value = '')
    {
        $value = is_array($value) ? implode(',', $value) : $value;
        return Form::tags($name, $options, $value);
    }
}
if (!function_exists('form_color')) {
    /**
     * @param array $options
     * @return string
     */

    function form_color($name = '', $options = [], $value = '')
    {
        return Form::color($name, $options, $value);
    }
}

if (!function_exists('form_label')) {
    /**
     * @param bool $reset
     * @param array $options
     * @return string
     */
    function form_label($label = '', $options = [])
    {
        return Form::label($label, $options);
    }
}
if (!function_exists('form_submitbtn')) {
    /**
     * @param bool $reset
     * @param array $options
     * @return string
     */
    function form_submitbtn($reset = true, $options = [])
    {
        return Form::submitbtn($reset, $options);
    }
}
if (!function_exists('form_closebtn')) {
    /**
     * @param bool $reset
     * @param array $options
     * @return string
     */
    function form_closebtn($reset = true, $options = [])
    {
        return Form::closebtn($reset, $options);
    }
}
if (!function_exists('form_upload')) {
    /**
     * @param $name
     * @param '' $formdata
     * @return string
     */
    function form_upload($name = '', $formdata = [], $options = [], $value = '')
    {
        return Form::upload($name, $formdata, $options, $value);
    }
}
if (!function_exists('form_editor')) {
    /**
     * @param $name
     * @return string
     */
    function form_editor($name = 'content', $type = 1, $options = [], $value = '')
    {
        return Form::editor($name, $type, $options, $value);
    }
}
if (!function_exists('form_selectpage')) {
    /**
     * @param $name
     * @return string
     */
    function form_selectpage($name = 'selectpage', $list = [], $options = [], $value=null)
    {
        return Form::selectpage($name, $list, $options, $value);
    }
}